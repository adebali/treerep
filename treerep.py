#!/usr/bin/env python

import sys
import os
from optparse import OptionParser
from ete3 import Tree
from common import *
from fastareader import fastareader


## Input&Output
parser = OptionParser()
parser.add_option("-i", "--input", dest="input", help="give us an input in nwk format", metavar="FILE")
parser.add_option("-f", "--fasta", dest="fasta", help="give us a fasta (optional)", metavar="FILE")
parser.add_option("-o", "--output", dest="output", help="give us an output (needed when --fasta)", metavar="FILE")
parser.add_option("-d", "--distance", dest="distanceCutoff", help="distance cutoff", metavar="num")
(options, args) = parser.parse_args()

assert options.input, "file (-i or --input) was not given"
if options.fasta:
	assert options.output, "output file (-o or --output) was not given"
## ##############

## Functions

def t2nodeList(input):
		t = Tree(input)
		theList = []
		for element in t.search_nodes():
				theList.append(element.name)
		return theList

def nwk2nodes(input):
	t = Tree(input)
	theList = []
	for element in t.search_nodes():
			theList.append(element)
	return theList

def node2representative(node,repNum=1):
	return node.get_closest_leaf()


t = Tree(options.input,format=3)
farthest, farthestDist = t.get_farthest_node()
if options.distanceCutoff:
	distanceCutoff = float(options.distanceCutoff)
else:
	distanceCutoff = float(farthestDist)/10

print("The distance cutoff used will be " + str(distanceCutoff))

nodes = nwk2nodes(options.input)
root = nodes[0]
assert root.is_root(), "root is not the first node"

nodeCount = 1
representativeList = []

for i in range(len(nodes)):
	currentNode = nodes[i]
	#print(currentNode.get_distance(root, topology_only=False))
	node_root_distance = currentNode.get_distance(root, topology_only=False)
	children = currentNode.children
	for child in children:
			child_dist = child.get_distance(root)
			if child_dist > distanceCutoff and node_root_distance < distanceCutoff:
				nodeCount += 1
				representativeList.append(node2representative(child)[0].get_leaf_names()[0])




if options.fasta:
	out = open(options.output,"w")
	d,l = fastareader(options.fasta)
	for rep in representativeList:
			out.write(">" + rep + "\n" + d[rep] + "\n")
	out.close()
else:
	for rep in representativeList:
		print(rep)