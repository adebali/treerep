import sys

def fastareader(datafile, just_name = 'no'):
        data = open(datafile,'r')
        seq_dic = {}
        list_order = []
        for line in data:
                line = line.replace('\r','')
                if line[0] == '>':
                        if just_name == 'Yes':
                                name = line.split('/')[0][1:]
                        else:
                                name = line[1:-1]
                        list_order.append(name)
                        seq_dic[name] = ''
                else:
                        while line.find('\n') != -1:
                                line = line[0:line.find('\n')] + line[line.find('\n')+2:]
                        seq_dic[name]= seq_dic[name] + line
#       dataout = ''
#       for k, v in seq_dic.iteritems():
#               dataout = dataout + '>' + k + '\n' + v + '\n'

        data.close()
        return seq_dic, list_order

