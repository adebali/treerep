# treerep - Tree Representatives

treerep (Tree Representatives) tool takes a phylogenetic tree (in newick format) as an input and outputs subset of sequence names representing their associated clades.

Optionally it also takes sequence (in fasta format) and outputs the representative set of sequences in fasta format. In this case, sequence headers and phylogenetic leaf names must match.

usage: 

```
python treerep.py -i input.nwk
```

optional:
```
python treerep.py -i input.newick -f sequences.fa -o reduced.fa
```


## Dependencies
* ETE3

![image.png](https://bitbucket.org/repo/xpr9ke/images/520014109-image.png)